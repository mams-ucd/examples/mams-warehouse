package generator.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.stereotype.Service;

import generator.model.Item;
import generator.model.Order;
import generator.model.OrderItem;

@Service
public class OrderService {
    private Map<Integer, Order> orders = new TreeMap<>();
    private Random random = new Random();
    private List<Item> items;

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Order generateOrder() {
        Order order = Order.newOrder();
        System.out.println("Creating order: #" + order.getId());
        Set<Integer> selected = new HashSet<>();
        int itemThreshold = 100;
        while (itemThreshold > 5 && random.nextInt(100) < itemThreshold) {
            // Select another unique item
            int index = random.nextInt(items.size());
            while (selected.contains(index)) random.nextInt(items.size());

            // Generate a quantity..
            int quantity = 1;
            int threshold = 33;
            while (threshold > 2 && random.nextInt(100) < threshold) {
                quantity++;
                threshold = threshold / 10;
            }
            OrderItem item = OrderItem.newOrderItem(items.get(index).getId(), quantity);
            order.getItems().add(item);
            System.out.println("Adding Item: " + item.getId() + " / " + item.getQuantity());
            itemThreshold = itemThreshold / 2;
        }

        orders.put(order.getId(), order);
        return order;
    }

    public Order getOrder(int id) {
        return orders.get(id);
    }

    public Collection<Order> getOrders() {
        return orders.values();
    }
}