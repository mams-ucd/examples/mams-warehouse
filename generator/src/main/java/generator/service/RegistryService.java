package generator.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import generator.model.Subscriber;

@Service
public class RegistryService {
    private List<Subscriber> subscribers = new LinkedList<>();

    public void register(Subscriber subscriber) {
        subscribers.add(subscriber);
    }

    public List<Subscriber> getSubscribers() {
        return subscribers;
    }
}