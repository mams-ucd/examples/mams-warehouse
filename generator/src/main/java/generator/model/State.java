package generator.model;

import org.springframework.stereotype.Service;

@Service
public class State {
    private int step;

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }
}