package generator.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import generator.model.Order;
import generator.service.OrderService;

@RestController
public class OrderController {
    @Autowired private OrderService orderService;

    @GetMapping("/orders")
    public Collection<Order> orders() {
        return orderService.getOrders();
    }

    @GetMapping("/orders/{id}")
    public Order order(@PathVariable int id) {
        return orderService.getOrder(id);
    }

    @PatchMapping("/orders/{id}")
    public void update(@PathVariable int id, @RequestBody Order orderUpdate) {
        Order order = orderService.getOrder(id);
        order.setState(orderUpdate.getState());
    }
}