package generator.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import generator.model.Subscriber;
import generator.service.RegistryService;

@RestController
public class RegistryController {
    @Autowired private RegistryService registryService;


    @PostMapping("/registry")
    public void step(@RequestBody Subscriber subscriber) {
        registryService.register(subscriber);
    }

    @GetMapping("/registry")
    public List<Subscriber> step() {
        return registryService.getSubscribers();
    }
}