package generator.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import generator.model.Order;
import generator.model.State;
import generator.model.Subscriber;
import generator.service.OrderService;
import generator.service.RegistryService;


@RestController
public class ExecuteController {
    @Autowired private State currentState;
    @Autowired private OrderService orderService;
    @Autowired private RegistryService registryService;

    @PutMapping("/step")
    public void step(@RequestBody State state) {
        currentState.setStep(state.getStep());

        if (Math.random() < 0.2) {
            // Generate an Order
            Order order = orderService.generateOrder();

            // Disseminate order to subscribers...
            RestTemplate template = new RestTemplate();
            HttpEntity<Order> body = new HttpEntity<Order>(order);
            for (Subscriber subscriber : registryService.getSubscribers()) {
                URI uri;
                try {
                    uri = new URI(subscriber.getUri());
                    template.postForEntity(uri, body, String.class);
                    System.out.println("Sending order to: " + uri);
                    } catch (URISyntaxException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @GetMapping("/step")
    public State step() {
        return currentState;
    }
}