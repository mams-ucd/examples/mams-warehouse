package generator;

import java.net.URI;
import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import generator.model.Item;
import generator.service.OrderService;

@Component
public class GeneratorRunner implements CommandLineRunner {
    @Autowired private OrderService orderService;

    @Override
    public void run(String...args) throws Exception {
        RestTemplate template = new RestTemplate();

        // Load the items from the simulator
        System.out.println("Getting Items...");
        URI uri = new URI("http://localhost:8080/items");
        Item[] array = template.getForObject(uri, Item[].class);
        orderService.setItems(Arrays.asList(array));
        System.out.println("Items: " + Arrays.asList(array));

        // Register the generator with the clock...
        System.out.println("Registering with Clock...");
        Registration registration = new Registration();
        registration.setName("Simulator");
        registration.setUri("http://localhost:8081/step");

        uri = new URI("http://localhost:9000/registry");
        HttpEntity<Registration> body = new HttpEntity<Registration>(registration);
        template.postForEntity(uri, body, String.class);
        System.out.println("READY...");
    }
}