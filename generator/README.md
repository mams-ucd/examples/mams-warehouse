# mams-warehouse-generator

Service that simulates the receipt of orders from an e-commerce application. It is
a component of a larger [Distribution Center Simulator](/mams-ucd/examples/mams-warehouse).


## Core Service Behaviour

The service should generate upto 1 order should be created per time step
* There should be a 20% chance of an order being generated

If an order is to be generated, then there should be:
* a 100% chance of the order containing 1 item
* a 50% chance of the order containing a second item
* a 25%  chance of the order containing a third item
* a 13% chance of the order containing a fourth item
* a 6% chance of the order containing a fifth item

For each item, there should be:
* a 100% chance of there being 1 of each item
* a 33% chance of there being 2 of each item
* a 3% chance of there being 3 of each item
