# mams-warehouse-clock

This project implements a global, discrete time, clock for the simulation using Akka + the Akka HTTP Module.  It is a component of a larger [Distribution Center Simulator](/mams-ucd/examples/mams-warehouse).

The service consists of two actors:

* The Broker actor: Implements a pub-sub service that handles incoming subscription request from external services and also disseminates messages from the publisher.
* The Clock actor: Implements a fixed interval (2 seconds) tick message that is sent to the broker. The broker then disseminates the tick to all subscribers.

## Deploying the service
The can be compiled and run using maven:

`
 $ mvn clean compile exec:java
`

The service is deployed by default on port 9000.

## Registering with the service

To register with the service, you must send a HTTP POST request to the `/registry` endpoint with the following body:

```
{
    "name" : "My Service Name",
    "uri" : "http://host:port/my-endpoint"
}
```
``
The expected media type is `application/json`.

The name is used for identification purposes. The uri is the endpoint to which tick's are sent. The name of the endpoint does not matter, but the service must implement the contract described below to successfully receive the clock state.

## The Subscriber Contract

It is expected that any subscribing service will implement an endpoint that accepts only a HTTP PUT request with the following body:

```
{
    "step" : X
}
```

No constraints exist on what the service should do with the information when received.

`

