package model;

import java.util.LinkedList;
import java.util.List;

public class Order {
    private static int orderNumber = 0;

    private int id;
    private List<OrderItem> items;
    private String state;

    public static Order newOrder() {
        Order order = new Order();
        order.setId(orderNumber++);
        order.setItems(new LinkedList<OrderItem>());
        order.setState("NEW");
        return order;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }
}