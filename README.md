# mams-warehouse

Implementation of a distribution center simulator that uses MAMS to implement worker and management system behaviours. This project consists of 5 modules:

* **The Clock Service**: This module implements a global, discrete time, clock for the simulation using Akka. Details on how to run and use this service can be found [here](clock).
* **The Simulator Service**: This module implements the core simulation of the environment, it includes the product list, the floorplan, and the "bodies" of the workers. It is implemented using SpringBoot. Details on how to run and use this service can be found [here](simulator).
* **The Order Generator Service**: This module implements an order generator which simulate the receipt of customer orders. It is implemented using SpringBoot. Details on how to run and use this service can be found [here](generator).
* **The Worker Service**: This module implements the "brains" of the workers. It is implemented using MAMS-CArtAgO. Details on how to run and use this service can be found [here](workers).
* **The Management Service**: This module is responsible for receiving incoming orders and allocating tasks to workers. It is implemented using MAMS-CArtAgO. Details on how to run and use this service can be found [here](management).

## Running the simulator

* Start the Clock Service: `mvn clean compile exec:java`
* Start the Simulator: `mvn clean compile spring-boot:run`
* Start the Order Generator Service: `mvn clean compile spring-boot:run`
* Start the Management Service: `mvn clean compile astra:deploy`
* Start the Worker Service: `mvn clean compile astra:deploy`
