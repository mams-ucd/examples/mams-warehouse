package simulator.model;

public class ActionResponse {
    public static final ActionResponse CREATED = new ActionResponse(204, null);
    
    private int code;
    private Object content;

    public ActionResponse(int code, Object content) {
        this.code = code;
        this.content = content;
    }

    public ActionResponse() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }
}