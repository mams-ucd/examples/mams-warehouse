package simulator.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simulator.service.ItemService;
import simulator.service.LocationService;
import simulator.service.MapService;
import simulator.service.WorkerService;

@Service
public class ServiceContext {
    @Autowired public MapService mapService;
    @Autowired public WorkerService workerService;
    @Autowired public ItemService itemService;
    @Autowired public LocationService locationService;
}