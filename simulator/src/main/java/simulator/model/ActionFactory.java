package simulator.model;

public interface ActionFactory<T extends ActionExecutor> {
    T create();
}