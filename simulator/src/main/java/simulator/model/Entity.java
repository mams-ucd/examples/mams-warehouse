package simulator.model;

public class Entity {
    private String id;
    private String type;

    public Entity(String id, String type) {
        this.id = id;
        this.type = type;
    }

    public Entity() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}