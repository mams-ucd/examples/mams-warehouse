package simulator.model;

import java.util.List;
import java.util.Set;

public class MapView {
    private Set<String> vertices;
    private List<String> edges;

    public Set<String> getVertices() {
        return vertices;
    }

    public void setVertices(Set<String> vertices) {
        this.vertices = vertices;
    }

    public List<String> getEdges() {
        return edges;
    }

    public void setEdges(List<String> edges) {
        this.edges = edges;
    }
    
}