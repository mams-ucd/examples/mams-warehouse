package simulator.model;

public class OrderPart {
    private Item item;
    private int quantity;

    public OrderPart(Item item, int quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public OrderPart() {}

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    
}