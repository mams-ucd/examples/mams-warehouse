package simulator.model.action;

import java.util.List;

import org.springframework.stereotype.Service;

import simulator.model.ActionExecutor;
import simulator.model.Item;
import simulator.model.ServiceContext;
import simulator.model.Worker;

@Service
public class PickFromExecutor implements ActionExecutor {
    @Override
    public boolean execute(Worker worker, ServiceContext serviceContext, List<String> parameters) {
        if (parameters.size() != 2) {
            return false;
        }

        // Parameter is a shelf
        String shelf = parameters.get(0);
        int quantity = Integer.parseInt(parameters.get(1));

        String location = worker.getLocation();
        String itemId = serviceContext.locationService.itemAt(location+"/"+shelf);
        if (itemId != null) {
            Item item = serviceContext.itemService.getItem(itemId);
            return worker.addOrderPart(item, quantity);
        }
        return false;
    }    
}