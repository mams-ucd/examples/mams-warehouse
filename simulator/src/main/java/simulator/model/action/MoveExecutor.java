package simulator.model.action;

import java.util.List;

import org.springframework.stereotype.Service;

import simulator.model.ActionExecutor;
import simulator.model.ServiceContext;
import simulator.model.Worker;

@Service
public class MoveExecutor implements ActionExecutor {
    @Override
    public boolean execute(Worker worker, ServiceContext serviceContext, List<String> parameters) {
        if (parameters.size() != 1) {
            return false;
        }

        String target = parameters.get(0);
        if (!serviceContext.mapService.isAdjacent(worker.getLocation(), target)) {
            return false;
        }

        serviceContext.mapService.removeEntity(worker.getLocation(), worker.getName());
        serviceContext.mapService.addEntity(target, worker.getName(), "worker");
        worker.setLocation(target);
        return true;
    }    
}