package simulator.model.action;

import simulator.model.ActionFactory;

public class MoveFactory implements ActionFactory<MoveExecutor> {
    @Override
    public MoveExecutor create() {
        return new MoveExecutor();
    }
    
}