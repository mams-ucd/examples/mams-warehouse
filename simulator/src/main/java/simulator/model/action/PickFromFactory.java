package simulator.model.action;

import simulator.model.ActionFactory;

public class PickFromFactory implements ActionFactory<PickFromExecutor> {
    @Override
    public PickFromExecutor create() {
        return new PickFromExecutor();
    }
    
}