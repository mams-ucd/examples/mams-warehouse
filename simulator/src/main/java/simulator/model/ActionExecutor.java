package simulator.model;

import java.util.List;

public interface ActionExecutor {
    boolean execute(Worker worker, ServiceContext serviceContext, List<String> parameters);
}