package simulator.model;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import simulator.model.action.MoveFactory;
import simulator.model.action.PickFromFactory;

public class Worker {
    private static Map<String, ActionFactory<?>> executors = new TreeMap<>();
    static {
        executors.put("move", new MoveFactory());
        executors.put("pick", new PickFromFactory());
    }

    private String name;
    private String location;
    private int capacity;
    private List<Action> actions = new LinkedList<>();
    private Action nextAction;
    private int weight;
    private List<OrderPart> holding = new LinkedList<OrderPart>();
    private String notificationUri;

    public Worker(String name, String location, int capacity) {
        this.name = name;
        this.location = location;
        this.capacity = capacity;
    }

    public Worker() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public List<Action> getActions() {
        return actions;
    }

    public boolean execute(ServiceContext serviceContext, Action action) {
        ActionExecutor executor = executors.get(action.getType()).create();
        return executor.execute(this, serviceContext, action.getArguments());
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public Action getNextAction() {
        return nextAction;
    }

    public void setNextAction(Action nextAction) {
        this.nextAction = nextAction;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public List<OrderPart> getHolding() {
        return holding;
    }

    public void setHolding(List<OrderPart> holding) {
        this.holding = holding;
    }

    public boolean addOrderPart(Item item, int quantity) {
        if (item.getWeight()*quantity <= capacity) {
            holding.add(new OrderPart(item, quantity));
            return true;
        }
        return false;
    }

    public String getNotificationUri() {
        return notificationUri;
    }

    public void setNotificationUri(String notificationUri) {
        this.notificationUri = notificationUri;
    }
}