package simulator;

import java.io.InputStream;
import java.net.URI;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import simulator.model.Item;
import simulator.model.Worker;
import simulator.service.ItemService;
import simulator.service.LocationService;
import simulator.service.MapService;
import simulator.service.WorkerService;

@Component
public class SimulatorRunner implements CommandLineRunner {
    @Autowired private WorkerService workerService;
    @Autowired private MapService mapService;
    @Autowired private ItemService itemService;
    @Autowired private LocationService locationService;

    @Override
    public void run(String...args) throws Exception {
        // Load Simulator configuration here...
        ObjectMapper mapper = new ObjectMapper();
        InputStream in = getClass().getResourceAsStream("/sample.json");
        Configuration configuration = mapper.readValue(in, Configuration.class);

        mapService.configure(configuration);
        for (Worker worker : configuration.getWorkers()) {
            workerService.addWorker(worker);
        }

        for (Item item : configuration.getItems()) {
            String id = itemService.addItem(item);
            String location = mapService.getNextShelf();
            item.setLocation(location);
            locationService.setLocation(id, location);
        }

        Registration registration = new Registration();
        registration.setName("Simulator");
        registration.setUri("http://localhost:8080/step");

        RestTemplate template = new RestTemplate();
        URI uri = new URI("http://localhost:9000/registry");
        HttpEntity<Registration> body = new HttpEntity<Registration>(registration);
        template.postForEntity(uri, body, String.class);
    }
}