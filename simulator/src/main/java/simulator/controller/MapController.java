package simulator.controller;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import simulator.model.Entity;
import simulator.model.MapView;
import simulator.service.MapService;

@RestController
@RequestMapping("/map")
public class MapController {
    @Autowired private MapService mapService;

    @GetMapping("")
    public MapView map() {
        return mapService.getMap();
    }
    
    @GetMapping("/vertices")
    public Set<String> vertices() {
        return mapService.vertices();
    }
    
    @GetMapping("/vertices/{name}")
    public Collection<Entity> vertices(@PathVariable String name) {
        return mapService.getAtVertex(name);
    }

    @GetMapping("/vertices/{name}/opposite")
    public List<String> opposite(@PathVariable String name) {
        return mapService.getOpposites(name);
    }
}