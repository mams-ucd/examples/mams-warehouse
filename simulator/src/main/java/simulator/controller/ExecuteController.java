package simulator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import simulator.model.Action;
import simulator.model.ServiceContext;
import simulator.model.State;
import simulator.model.Worker;

@RestController
public class ExecuteController {
    @Autowired private ServiceContext serviceContext;
    @Autowired private State currentState;

    @PutMapping("/step")
    public void step(@RequestBody State state) {
        currentState.setStep(state.getStep());
        for (Worker worker : serviceContext.workerService.getWorkers()) {
            Action action = worker.getNextAction();
            if (action != null) {
                action.setSuccess(worker.execute(serviceContext, action));
                action.setStep(state.getStep());
                worker.getActions().add(action);
                worker.setNextAction(null);
            }
        }
    }

    @GetMapping("/step")
    public State step() {
        return currentState;
    }
}