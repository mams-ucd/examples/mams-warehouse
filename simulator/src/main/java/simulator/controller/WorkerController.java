package simulator.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import simulator.model.Action;
import simulator.model.Worker;
import simulator.service.WorkerService;

@RestController
public class WorkerController {
    @Autowired private WorkerService workerService;
    
    @GetMapping("/workers")
    public Set<String> workers() {
        return workerService.getWorkerNames();
    }

    @GetMapping("/workers/{name}")
    public Worker workers(@PathVariable String name) {
        return workerService.getWorker(name);
    }

    @PutMapping("/workers/{name}/nextAction")
    public void act(@PathVariable String name, @RequestBody Action action) {
        workerService.getWorker(name).setNextAction(action);
    }

    @GetMapping("/workers/{name}/actions")
    public List<Action> actions(@PathVariable String name) {
        return workerService.getWorker(name).getActions();
    }
    @PatchMapping("/workers/{name}")
    public void notificationPatch(@PathVariable String name, @RequestBody Worker worker) {
        workerService.getWorker(name).setNotificationUri(worker.getNotificationUri());
    }
}