package simulator.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import simulator.model.Item;
import simulator.model.Location;
import simulator.service.ItemService;
import simulator.service.LocationService;

@RestController
public class ItemController {
    @Autowired private ItemService itemService;
    @Autowired private LocationService locationService;

    @GetMapping("/items")
    public Collection<Item> items() {
        return itemService.getItems();
    }

    @GetMapping("/items/{id}")
    public Item item(@PathVariable String id) {
        return itemService.getItem(id);
    }

    @GetMapping("/items/locations")
    public Collection<Location> locations() {
        return locationService.getLocations();
    }}