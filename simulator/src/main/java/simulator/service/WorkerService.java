package simulator.service;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import simulator.model.Worker;

@Service
public class WorkerService {
    @Autowired MapService mapService;

    private Map<String, Worker> workers = new TreeMap<>();

    public void addWorker(Worker worker) {
        workers.put(worker.getName(), worker);
        mapService.addEntity(worker.getLocation(), worker.getName(), "worker");
    }

    public Worker getWorker(String name) {
        return workers.get(name);
    }

    public Set<String> getWorkerNames() {
        return workers.keySet();
    }

    public Collection<Worker> getWorkers() {
        return workers.values();
    }
}