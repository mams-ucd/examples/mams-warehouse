package simulator.service;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Service;

import simulator.model.Item;

@Service
public class ItemService {
    private Map<String, Item> items = new TreeMap<>();

    public String addItem(Item item) {
        items.put(item.getId(), item);
        return item.getId();
    }

    public Collection<Item> getItems() {
        return items.values();
    }

    public Item getItem(String id) {
        return items.get(id);
    }
}