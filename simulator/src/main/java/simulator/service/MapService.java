package simulator.service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;
import org.springframework.stereotype.Service;

import simulator.Configuration;
import simulator.model.Entity;
import simulator.model.MapView;

@Service
public class MapService {
    private Graph<String, DefaultEdge> graph = new SimpleGraph<>(DefaultEdge.class);
    private Map<String, Map<String, Entity>> entities = new TreeMap<>();
    private Queue<String> freeShelves = new LinkedList<>();
    public MapService() {
    }

    public void configure(Configuration configuration) {
        for (int i = 1; i <= configuration.getAisles(); i++) {
            // add first vertex (outside of aisle)
            String vertex = "a" + i + ".0";
            addVertex(vertex);
            if (i > 1) {
                graph.addEdge(vertex, "a" + (i - 1) + ".0");
            }

            // add vertices in aisle
            for (int j = 1; j <= configuration.getSections(); j++) {
                String nextVertex = "a" + i + "." + j;
                addVertex(nextVertex);
                graph.addEdge(vertex, nextVertex);

                // add shelving here...
                for (int k = 0; k < configuration.getShelves(); k++) {
                    String shelf =Character.toString((char) ('A' + k));
                    freeShelves.add(nextVertex+"/"+shelf);
                    addEntity(nextVertex, shelf, "shelf");
                }

                vertex = nextVertex;
            }

            // add last vertex (outside of aisle)
            vertex = "a" + i + "." + (configuration.getSections() + 1);
            addVertex(vertex);
            if (i > 1) {
                graph.addEdge(vertex, "a" + (i - 1) + "." + (configuration.getSections() + 1));
            }
        }
    }

    public MapView getMap() {
        MapView map = new MapView();
        map.setVertices(graph.vertexSet());
        List<String> edges = new LinkedList<>();
        graph.edgeSet().stream().forEach(edge -> {
            edges.add(graph.getEdgeSource(edge) + "->" + graph.getEdgeTarget(edge));
        });
        map.setEdges(edges);
        return map;

    }

    public void addVertex(String vertex) {
        graph.addVertex(vertex);
        entities.put(vertex, new TreeMap<String, Entity>());
    }

    public void addEntity(String vertex, String name, String type) {
        Map<String, Entity> vertexEntities = entities.get(vertex);
        if (vertexEntities == null)
            throw new RuntimeException("No Such Vertex: " + vertex);
        vertexEntities.put(name, new Entity(name, type));
    }

    public void removeEntity(String vertex, String name) {
        Map<String, Entity> vertexEntities = entities.get(vertex);
        vertexEntities.remove(name);
    }

    public Set<String> vertices() {
        return graph.vertexSet();
    }

    public Collection<Entity> getAtVertex(String vertex) {
        return entities.get(vertex).values();
    }

    public boolean isAdjacent(String location, String target) {
        for (DefaultEdge edge : graph.edgesOf(location)) {
            if (graph.getEdgeSource(edge).equals(target) || graph.getEdgeTarget(edge).equals(target)) return true;
        }
        return false;
    }
    
    public String getNextShelf() {
        return freeShelves.remove();
    }

	public List<String> getOpposites(String location) {
        List<String> opposites = new LinkedList<>();
		for (DefaultEdge edge : graph.edgesOf(location)) {
            if (graph.getEdgeSource(edge).equals(location)) {
                opposites.add(graph.getEdgeTarget(edge));
            } else {
                opposites.add(graph.getEdgeSource(edge));
            }

        }
        return opposites;
	}
}