package simulator.service;

import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.stereotype.Service;

import simulator.model.Location;

@Service
public class LocationService {
    private Map<String, Location> locations = new TreeMap<>();

    public void setLocation(String id, String location) {
        locations.put(location, new Location(id, location));
    }

    public String itemAt(String location) {
        Location l = locations.get(location);
        if (l == null) return null;
        return l.getId();
    }

    public Collection<Location> getLocations() {
        return locations.values();
    }
}