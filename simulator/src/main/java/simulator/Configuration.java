package simulator;

import java.util.List;
import simulator.model.Item;
import simulator.model.Worker;

public class Configuration {
    private int aisles = 2;
    private int sections = 4;
    private int shelves = 1;
    private List<Worker> workers;
    private List<Item> items;
    private List<String> packagingAreas;

    public int getAisles() {
        return aisles;
    }

    public void setAisles(int aisles) {
        this.aisles = aisles;
    }

    public int getSections() {
        return sections;
    }

    public void setSections(int sections) {
        this.sections = sections;
    }

    public int getShelves() {
        return shelves;
    }

    public void setShelves(int shelves) {
        this.shelves = shelves;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(List<Worker> workers) {
        this.workers = workers;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<String> getPackagingAreas() {
        return packagingAreas;
    }

    public void setPackaginAreas(List<String> packagingAreas) {
        this.packagingAreas = packagingAreas;
    }

}