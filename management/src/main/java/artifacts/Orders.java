package artifacts;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import cartago.Op;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import mams.artifacts.ResourceArtifact;
import mams.utils.CartagoBackend;
import mams.utils.Utils;
import mams.web.WebServer;

public class Orders extends ResourceArtifact {

    private static ObjectMapper mapper = new ObjectMapper();


    @Override
    public boolean handle(ChannelHandlerContext ctxt, FullHttpRequest request) {
        if (request.method().asciiName().toString().equals("POST")) {

            try {
                JsonNode node = mapper.readTree(Utils.getBody(request));
                CartagoBackend.getInstance().doAction(this.getId(), new Op("receive", node.get("performative").asText(),
                        node.get("sender").asText(), mapper.writeValueAsString(node.get("content"))));
                WebServer.writeResponse(ctxt, request, HttpResponseStatus.OK, "application/json", "");
            } catch (Exception e) {
                WebServer.writeResponse(ctxt, request, HttpResponseStatus.INTERNAL_SERVER_ERROR, "plain/text", e.getMessage());
                e.printStackTrace();
            }

        }else if(request.method().asciiName().toString().equals("GET")){
            try {
               
            } catch (Exception e) {
                WebServer.writeResponse(ctxt, request, HttpResponseStatus.INTERNAL_SERVER_ERROR, "plain/text", e.getMessage());
                e.printStackTrace();
            }
        } else {
            WebServer.writeErrorResponse(ctxt, request, HttpResponseStatus.METHOD_NOT_ALLOWED);
        }

        return false;
    }
    
}
