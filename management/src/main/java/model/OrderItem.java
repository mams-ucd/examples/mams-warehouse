package model;

public class OrderItem {
    private String id;
    private int quantity;

    public static OrderItem newOrderItem(String id, int quantity) {
        OrderItem item = new OrderItem();
        item.setId(id);
        item.setQuantity(quantity);
        return item;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}